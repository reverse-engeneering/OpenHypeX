import usb.core
import productInfo as products
import writing
from terminalColors import tcolor

detectedProducts = []

def customFilter(dev):
    if(hex(dev.idVendor) == hex(0x0951)):
        return True

for dev in usb.core.find(find_all=True, custom_match=customFilter):
    detectedProducts.append(dev)
    
if len(detectedProducts) == 0:
    print('Устройства HyperX не обнаружены.')
    exit()
else:
    print("Обнаружены устройства HyperX", end="")
    index = 0
    string = ':\n\n'
    for cfg in dev:
        for product in products.supported:
            if(hex(product.id) == hex(cfg.device.idProduct)):
                index+=1
                string += '[' + str(index) + '] ' + product.name + '\n'
    if index == 0:
        print(', но ни одино из них ещё не поддерживается программой или не имеет настроек.')
        exit()
    print(string)


index = int(input('Введите номер устройства: ')) - 1
dev = detectedProducts[index]
print(tcolor.blue + 'Вы выбрали ' + dev.product + tcolor.endc)

if(dev.product == 'HyperX Pulsefire Haste'):
    def formatColor(c):
        if(c > 255): c = 255
        if(c < 0): c = 0
        if(c < 16):
            c = bytes.fromhex('0' + format(c,'x'))
        else:
            c = bytes.fromhex(format(c, 'x'))
        return c

    option = int(input('1: Ввести цвет\n2: Загрузить из файла\n'))

    if option == 1:
        r = int(input("Введите " + tcolor.red + "красный(0-255): " + tcolor.endc))
        g = int(input("Введите " + tcolor.green + "зелёный(0-255): " + tcolor.endc))
        b = int(input("Введите " + tcolor.blue + "синий(0-255): " + tcolor.endc))
        file = open("hyperx.bin", "w")
        file.write(str(r)+"\n")
        file.write(str(g)+"\n")
        file.write(str(b))
        file.close()
        r = formatColor(r)
        g = formatColor(g)
        b = formatColor(b)
        writing.tempColor(dev, r, g, b)
    
    if option == 2:
        file = open('hyperx.bin')
        settings = file.read().splitlines()
        settingsBytes = []
        for string in settings:
            settingsBytes.append(formatColor(int(string)))
        writing.tempColor(dev, settingsBytes[0], settingsBytes[1], settingsBytes[2])