class Product:
    def __init__(self, _name, _id):
        self.name = _name
        self.id = _id
    
    def __str__(self):
        return f"{self.name}({self.id})"
    
supported = [
    Product('Pulsefire Haste', 0x1727),
    Product('7.1 Audio', 0x16a4)
]